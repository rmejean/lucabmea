# Land Use Change Agent-Based-Model for Ecuadorian Amazon (LUCABMEA)  <img src="logo.png" align="right" width="150" />
An agent-based model of deforestation dynamics in NEA for my thesis.
This model is built in GAML ([GAMA-Platform](https://gama-platform.github.io/) Language).


**Dependencies:** For synthetic population generation and the generation of an agricultural landscape, this model uses [GAMA plugin of the GENSTAR project](https://github.com/ANRGenstar/genstar.gamaplugin).

- Version : 1.0
- Author : Romain Mejean, PhD student in Geography @t UMR 5602 GEODE CNRS/Université Toulouse 2 Jean Jaurès
- Contact : romain.mejean@univ-tlse2.fr / [romainmejean.fr](http://romainmejean.fr)
- Year : 2019-2023

Work in progress, ODD protocol coming soon... ;)
